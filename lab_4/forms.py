from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['to', 'fr', 'title', 'message']

    to_attrs = {
        'class':'fields',
        'type':'text',
        'placeHolder':'To*'
    }

    fr_attrs = {
        'class':'fields',
        'type':'text',
        'placeHolder':'From*'
    }
    
    title_attrs = {
        'class':'fields',
        'type':'text',
        'placeHolder':'Title*'
    }

    msg_attrs = {
        'class':'fields',
        'placeHolder':'Message*'
    }

    to = forms.CharField(label=False, required=True, max_length=30, widget=forms.TextInput(attrs=to_attrs))
    fr = forms.CharField(label=False, required=True, max_length=30, widget=forms.TextInput(attrs=fr_attrs))
    title = forms.CharField(label=False, required=True, max_length=50, widget=forms.TextInput(attrs=title_attrs))
    message = forms.CharField(label=False, required=True, max_length=300, widget=forms.Textarea(attrs=msg_attrs))