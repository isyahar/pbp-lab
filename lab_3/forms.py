from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'DOB']

    name_attrs = {
        'type':'text',
        'placeHolder':'Type Name'
    }

    npm_attrs = {
        'type':'text',
        'placeHolder':'Type NPM'
    }
    
    dob_attrs = {
        'type':'date'
    }

    name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    npm = forms.CharField(label='NPM', required=True, max_length=10, widget=forms.TextInput(attrs=npm_attrs))
    DOB = forms.DateField(label='DOB', required=True, widget=forms.DateInput(attrs=dob_attrs))
