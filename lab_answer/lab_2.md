Isyah Auliarahmani Rafifa
NPM 2006464221
Tugas Individu 4 PBP-F

1. Apakah perbedaan antara JSON dan XML?
JSON hanya merupakan format data, sementara XML merupakan markup language. JSON lebih pendek, sederhana, dan mudah dibaca dibandingkan dengan XML. Di sisi lain, XML lebih cocok untuk digunakan dalam pertukaran data yang kompleks daripada JSON. JSON digunakan untuk bertukar data sederhana, sementara XML digunakan untuk menyimpan dan mengirimkan data antaraplikasi. Data JSON disimpan dalam format key-pair value, sementara XML dalam bentuk tree structure. JSON mendukung tipe data yang berbeda, sedangkan XML hanya memiliki data bertipe string. Tampilan sintaks pada JSON tidak bisa diubah, sedangkan XML bisa karena merupakan XML merupakan sebuah markup language. XML menampilkan datanya dalam struktur tag sehingga lebih mudah untuk dibaca oleh mesin dibandingkan dengan JSON. JSON didukung oleh hampir seluruh browser, sedangkan browser yang mendukung XML jumlahnya lebih terbatas. 

2. Apakah perbedaan antara HTML dan XML?
Keduanya berhubungan, dimana HTML menampilkan data dan menguraikan struktur dari sebuah webpage sementara XML menyimpan dan mengirimkan data. HTML adalah markup language, sementara XML adalah markup language standar yang mendefinisikan markup language lain. Closing tags pada elemen HTML umumnya tidak harus selalu ada, sementara elemen XML harus memiliki closing tags. Whitespace pada file HTML tidak memengaruhi tampilan HTML pada webpage, sementara whitespace pada XML dapat berpengaruh. HTML membuat sebuah webpage dapat ditampilkan pada client-side, sementara XML memungkinkan adanya perpindahan data dari database dan aplikasi terkait. HTML termasuk static file, sementara XML termasuk dynamic file.


Referensi:
Risyan, Resa. (2020, December 21). Apa Perbedaan JSON Dan XML?. Retrieved from https://www.monitorteknologi.com/perbedaan-json-dan-xml/.
Vats, Rohan. (2021, Jan 4). HTML Vs XML: Difference Between HTML and XML [2021]. Retrieved from https://www.upgrad.com/blog/html-vs-xml/#:~:text=HTML%20and%20XML%20are%20related,language%20that%20defines%20other%20languages.